package p_Ennemy;

import java.awt.Color;
import java.awt.Dimension;
import java.awt.Graphics;
import java.awt.Image;
import java.awt.List;
import java.awt.event.FocusAdapter;
import java.awt.event.FocusEvent;
import java.awt.event.KeyEvent;
import java.awt.event.KeyListener;
import java.awt.event.MouseAdapter;
import java.awt.event.MouseEvent;
import java.awt.image.BufferedImage;
import java.io.File;
import java.io.IOException;

import javax.imageio.ImageIO;
import javax.swing.ImageIcon;
import javax.swing.JLabel;
import javax.swing.JPanel;
import javax.swing.JTextField;
import javax.swing.JLayeredPane;

public class BlocVisu extends JPanel {
	private Bloc bloc;
	private JTextField info;

	private IGrille laGrille;
	
//	private String Dir = "V";

	// http://www.java2s.com/Code/Java/Swing-JFC/Panelwithbackgroundimage.htm
	static private Image fond = new ImageIcon(".\\mer.jpg").getImage();

	// Image panel = new Image(new
	// ImageIcon("images/background.png").getImage())

	private int nbBloc = 3;

	public Bloc getBloc() {
		return bloc;
	}

	public void setBloc(Bloc bloc) {
		this.bloc = bloc;
	}

	public void setEtat(int etat) {
		bloc.setEtat(etat);
		info.setText(bloc.getX() + "," + bloc.getY() + "=" + bloc.getEtat());
		// ///this.setBackground(Color.BLUE);
	}

	public void setVisible(boolean v) {
		info.setVisible(v);
	}

	public void setBground(Color c) {
		this.setBackground(c);
	}

	/**
	 * @wbp.parser.constructor
	 */
//	public BlocVisu(Image img) {
//		//this.fond = img;
//		Dimension size = new Dimension(img.getWidth(null), img.getHeight(null));
//		setPreferredSize(new Dimension(252, 130));
//		setMinimumSize(size);
//		setMaximumSize(size);
//		setSize(size);
//		setLayout(null);
//	}
	
	
	
//	/** Surcharge de la fonction paintComponent() pour afficher notre image */
//    protected void paintComponent(Graphics g) {
//           
//           g.drawImage(wPic,0,0,null);
//   }
	
	private BufferedImage myPicture;
	private BufferedImage wPic;
	public BlocVisu(int pos, int etat, IGrille _laGrille) {


//		try {
//			wPic = ImageIO.read(this.getClass().getResource("mer.jpg"));
//		} catch (IOException e2) {
//			// TODO Auto-generated catch block
//			e2.printStackTrace();
//		}
//		JLabel wIcon = new JLabel(new ImageIcon(wPic));
//		try {
//			myPicture = ImageIO.read(new File("mer.jpg"));
//		} catch (IOException e1) {
//			// TODO Auto-generated catch block
//			e1.printStackTrace();
//		}
//		JLabel picLabel = new JLabel(new ImageIcon(myPicture));
//		add(picLabel);
		laGrille = _laGrille;
		
		
		int y = laGrille.getY(pos);
		int x = laGrille.getX(pos);
		bloc = new Bloc(x, y);
		bloc.setEtat(etat);

		info = new JTextField(bloc.getX() + "," + bloc.getY() + "="	+ bloc.getEtat());
		info.setEditable(false);
		info.setEnabled(false);
		// ////////////info.setVisible(true);

		this.add(info);

		// MouseAdapter m = new MouseAdapter();
		// m.mouseClicked(e);
		this.addMouseListener(new MouseAdapter() {
			@Override
			public void mouseClicked(MouseEvent e) {
				Bloc b = getBloc();
				if (b.getEtat() == 1) {
					b.setEtat(0);
					info.setText("touche");
					setBground(b.getColor());

				} else {
					if (!info.getText().equals("touche")) {
						info.setText("Clic");
						setVisible(true);
						setBground(b.getColor());
						// info.repaint();
					}
				}
			}

			@Override
			public void mouseEntered(MouseEvent e) {
				setBground(Color.YELLOW);

				// -----------------
				for (BlocVisu b : laGrille.getBlocs(laGrille.getDir(), getBloc().getX(), getBloc().getY(), nbBloc)) {
					if (b != null)
						b.setBground(Color.YELLOW);
				}

				///System.out.println(getBloc().getX());
				
				
				// System.out.println(e.getX());
			}

			@Override
			public void mouseExited(MouseEvent e) {
				setBground(Color.LIGHT_GRAY);
				// if (blocs != null)
				// blocs.setBground(Color.LIGHT_GRAY);
				// Bloc b = getBloc();
				// setBground(b.getColor());

				for (BlocVisu b : laGrille.getBlocs(laGrille.getDir(), getBloc().getX(),
						getBloc().getY(), nbBloc)) {
					if (b != null)
						b.setBground(b.bloc.getColor());
				}

			}

		});

//		this.addKeyListener(new KeyListener() {
//			@Override
//			public void keyPressed(KeyEvent e) {
//				System.out.println("tester");
//			}
//
//			@Override
//			public void keyReleased(KeyEvent e) {
//				System.out.println("2test2");
//			}
//
//			@Override
//			public void keyTyped(KeyEvent e) {
//				System.out.println("3test3");
//			}
//		});
//		this.requestFocus();
//		info.requestFocusInWindow();

	}



}
