package p_Ennemy;

import java.awt.Color;

public class Bloc {
	private int x;
	private int y;
	
	/**
	 * 1 = pas touché (valide)
	 * 0 = touché
	 */
	private int etat;
	
	public Bloc(int i, int j) {
		setX(i);
		setY(j);
		
		try{
			setEtat(1);	
		}catch(Exception ex){
			System.out.println(ex.getMessage());
		}
		
	}
	public int getX() {
		return x;
	}
	public void setX(int x) {
		this.x = x;
	}
	public int getY() {
		return y;
	}
	public void setY(int y) {
		this.y = y;
	}
	public int getEtat() {
		return etat;
	}
	public Color getColor() {
		if(getEtat()==0)
			return Color.RED;	
		else if (getEtat()==1)
			return Color.lightGray;
		else
			return Color.lightGray;
	}
	
	
	public void setEtat(int etat) {
		this.etat = etat;
	}
	
	
	public int isTouche(int x, int y){
		if (this.x==x && this.y==y){
			setEtat(0);
		}
		return getEtat();
	}

}
