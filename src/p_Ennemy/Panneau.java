package p_Ennemy;

import java.awt.Color;
import java.awt.GridLayout;
import java.awt.event.ActionEvent;
import java.awt.event.MouseAdapter;
import java.awt.event.MouseEvent;
import java.beans.PropertyChangeListener;
import java.util.Observable;
import java.util.Observer;

import javax.swing.Action;
import javax.swing.JComponent;
import javax.swing.JPanel;
import javax.swing.JTextField;
import javax.swing.JLabel;
import javax.swing.JRadioButton;
import javax.swing.GroupLayout;
import javax.swing.GroupLayout.Alignment;
import javax.swing.border.BevelBorder;

import java.awt.Component;
import java.awt.FlowLayout;

import javax.swing.border.LineBorder;

import java.awt.CardLayout;
import java.awt.event.KeyEvent;

public class Panneau extends JPanel implements Observer {
	private JTextField textField;
	private IJoueur joueur;
	private JTextField textField_nb;
	
	
	@Override
	public void update(Observable o, Object arg) {
		//if (o == textField) {
			System.out.println("ffffffffffff" + textField.getText());
		//}
		
	}
	public Panneau() {
		setOpaque(false);
		setAlignmentY(Component.TOP_ALIGNMENT);
		setAlignmentX(Component.LEFT_ALIGNMENT);
		setBorder(new LineBorder(Color.RED, 3, true));
		constructor();
	}


	public Panneau(IJoueur joueur) {
		//super();

		this.joueur = joueur;
		constructor();
	}

    public void constructor() {
		JLabel lblNomDuJoueur = new JLabel();
		lblNomDuJoueur.setText("Nom du joueur");
		
		textField = new JTextField();
		textField.setText(joueur.getNom());
		textField.setColumns(10);
		
		JLabel lblNbBatimentsFlotte = new JLabel("nb batiments flotte");
		
		
		textField_nb = new JTextField();
		textField_nb.setText(joueur.getMaFlotte().getNbBatiment() + "");
		textField_nb.setColumns(10);
		
		JRadioButton rdbtnBloc_1 = new JRadioButton("1 bloc");
		rdbtnBloc_1.setEnabled(false);
		
		
		JRadioButton rdbtnBloc_2 = new JRadioButton("2 blocs");
		rdbtnBloc_2.setMnemonic(KeyEvent.VK_2);

		JRadioButton rdbtnBloc_3 = new JRadioButton("3 blocs");
		rdbtnBloc_3.setMnemonic(KeyEvent.VK_3);

		JRadioButton rdbtnBloc_4 = new JRadioButton("4 blocs");
		rdbtnBloc_4.setMnemonic(KeyEvent.VK_4);
		
		
		
		Action a = new Action() {
			@Override
			public void actionPerformed(ActionEvent e) {
				System.out.println();
				
			}
			
			@Override
			public void setEnabled(boolean b) {
				this.setEnabled(b);
				// TODO Auto-generated method stub
				
			}
			
			@Override
			public void removePropertyChangeListener(PropertyChangeListener listener) {
				// TODO Auto-generated method stub
				
			}
			
			@Override
			public void putValue(String key, Object value) {
				// TODO Auto-generated method stub
				
			}
			
			@Override
			public boolean isEnabled() {
				// TODO Auto-generated method stub
				return true;
			}
			
			@Override
			public Object getValue(String key) {
				// TODO Auto-generated method stub
				return null;
			}
			
			@Override
			public void addPropertyChangeListener(PropertyChangeListener listener) {
				// TODO Auto-generated method stub
				
			}
		};
		rdbtnBloc_4.setSelected(true);

		rdbtnBloc_1.setAction(a);
		rdbtnBloc_2.setAction(a);
		rdbtnBloc_3.setAction(a);
		rdbtnBloc_4.setAction(a);
		
		setLayout(new GridLayout(0, 1, 0, 0));
		add(lblNomDuJoueur);
		add(textField);
		add(lblNbBatimentsFlotte);
		add(textField_nb);
		add(rdbtnBloc_1);
		add(rdbtnBloc_2);
		add(rdbtnBloc_3);
		add(rdbtnBloc_4);
		
		
		// MouseAdapter m = new MouseAdapter();
		// m.mouseClicked(e);
		this.addMouseListener(new MouseAdapter() {
			@Override
			public void mouseClicked(MouseEvent e) {
			}

			@Override
			public void mouseEntered(MouseEvent e) {
				System.out.println("in");
				setEnabledFocus(true);

			
			}

			@Override
			public void mouseExited(MouseEvent e) {
				System.out.println("out");
				setEnabledFocus(false);

			}

		});
		
		

	}
    
    public void setEnabledFocus(boolean active) {
//    	this.setEnabled(active);
//    	for (Component comp : this.getComponents()) {
//			comp.setEnabled(active);
//		}
    }

}
