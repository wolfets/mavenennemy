package p_Ennemy;

import java.util.Observable;
import java.util.Observer;

public class TextObserver extends Observable implements Observer {
	private ObservableValue ov = null;

	public TextObserver(ObservableValue ov) {
		this.ov = ov;
	}

	@Override
	public void update(Observable o, Object arg) {
		if (o == ov) {
			System.out.println(ov.getValue());
		}
		
	}
}
