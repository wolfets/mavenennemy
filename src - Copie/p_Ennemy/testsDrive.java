package p_Ennemy;

import java.util.ArrayList;
import java.util.List;

public class testsDrive {
	
	public static void main(String[] arg){
		System.out.println("// bloc : istouche, existe, new,-------------"); 
		testBlocisTouche();
		System.out.println("");
		
		System.out.println("// batiment : new, addBloc,  isCoule,-------------"); 
		testBatimentaddBloc();
		testBatimentIsTouche();
		testBatimentGetBloc();
		System.out.println("");
		
		
		
		System.out.println("// Flotte : addBatiment, isCoule,-------------"); 
		testFlotteaddBatiment();
		testFlotteCoule();
		testFlotteReste();
		
		
		System.out.println("");
		
		
		 
		System.out.println("// Joueur : Tir,-------------"); 
		testJoueurTir();
		
		
		
		//testJoueurHit();
		
	}

	private static Flotte testFlotteReste() {
		String fct = "FlotteReste";
		Flotte f = new Flotte();
		
		Batiment b = new Batiment("V",10,10,3);
		f.addBatiment(b);
		
		int reste = f.getReste();
		if(reste<1){
			System.out.println(fct + "...KO!!!" + reste);
		} else {
			System.out.println(fct + "...ok" + reste);
		}
		return f;
		
	}

	private static void testBatimentGetBloc() {
		String fct = "BatimentGetBloc";
		Batiment bat = new Batiment("V", 10, 10, 3);

		Bloc b = bat.getBloc(10,10);
		if(b!=null){
			System.out.println(fct + "...ok");
		} else {
			System.out.println(fct + "...KO!!!");
		}
		
		
	}

	private static void testJoueurTir() {
		String fct = "JoueurTir";
		Flotte f = new Flotte();
		Batiment b = new Batiment("V", 10, 10, 3);
		f.addBatiment(b);

		boolean r = f.isBatimentTouche(10,10);
		if(r){
			System.out.println(fct + "...ok");
		} else {
			System.out.println(fct + "...KO!!!");
		}
		
	}

	private static void testFlotteCoule() {
		String fct = "FlotteCoule";
		Flotte f = new Flotte();
		Batiment b = new Batiment("V", 10, 10, 3);
		f.addBatiment(b);
		
		Joueur j = new Joueur();
		j.tir(10, 10, f);
		j.tir(10, 11, f);
		j.tir(10, 12, f);
		
		boolean r = f.isFlotteCoule();
		if(r){
			System.out.println(fct + "...ok");
		} else {
			System.out.println(fct + "...KO!!!");
		}
		
	}

	private static void testBatimentIsTouche() {
		String fct = "BatimentIsTouche";
	
		Batiment bt = new Batiment();
		Bloc b = new Bloc(10,10);
		int s = bt.addBloc(b).size();
		
		boolean r1 = false, r2 = false;
		r1 = bt.isBlocTouche(20, 10);
		r2 = bt.isBlocTouche(10, 10);
		
		if(r1==false && r2==true){
			System.out.println(fct + "...ok");
		} else {
			System.out.println(fct + "...KO!!!" + r1 + "," + r2);
		}
		
	}

	private static Flotte testFlotteaddBatiment() {
		String fct = "FlotteaddBatiment";
		Flotte f = new Flotte();
		
		Batiment b = new Batiment();
		f.addBatiment(b);
		
		if(f.getBatiments().size()<=0){
			System.out.println(fct + "...KO!!!");
		} else {
			System.out.println(fct + "...ok");
		}
		return f;
		
	}

	private static void testBatimentaddBloc() {
		String fct = "BatimentaddBloc";
		Batiment bt = new Batiment();
		Bloc b = new Bloc(10,10);
		int s = bt.addBloc(b).size();
		
		if (s<=0) {
			System.out.println(fct + "...KO!!!");
		} else {
			System.out.println(fct + "...ok");
		}
		
	}

	private static void testJoueurHit() {
		String fct = "JoueurHit";
		Joueur j = new Joueur();
		int h = j.hit();
		if (h > 10) {
			System.out.println(fct + "...KO!!!");
		} else {
			System.out.println(fct + "...ok");
		}
		
	}

	private static void testBlocisTouche() {
		String fct = "BlocisTouche";
		Bloc b = new Bloc(6, 10);
		
		int reste = b.isTouche(6, 10);
		if(reste==0)
			System.out.println(fct + "...ok");
		else
			System.out.println(fct + "...KO!!!");
	}
	
	

}
